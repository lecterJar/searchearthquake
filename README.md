**BackEndChallenge**


**if you try to run it locally, need to run:**

* pip3 install virtualenv
* virtualenv earthquake  --python=python3.7
* source ./earthquake/bin/activate
* pip3 install -r requirements.txt


**run this commands to start the code:**

* python3.7 app.py    # As development and debug 
