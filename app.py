import requests
import json
import sqlite3
import datetime

from flask import Flask, request
app = Flask(__name__)

@app.route("/searchEarthquake/getEarthquakesByDates", methods=['POST'])
def getEarthquakesByDates() :

    try:
        conn = sqlite3.connect(':memory')

        # Create table
        conn.execute('''CREATE TABLE IF NOT EXISTS earthquake
                    (create_at timestamp PRIMARY KEY, 
                    fecha_inicio timestamp, 
                    fecha_fin timestamp, 
                    magnitud_min real , 
                    magnitud_max real,
                    salida text)''')
        
        req_data = request.get_json()
        fecha_inicio = req_data['fechaInicio']
        fecha_fin = req_data['fechaFin']
        magnitude_minima = req_data['magnitudeMinima']
        response = requests.get('https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime={}&endtime={}&minmagnitude={}'.format(fecha_inicio,fecha_fin,magnitude_minima), verify=False)
        data_json = json.loads(response.text)
        largo = len(data_json["features"])
        finalList = []

        for i in range(largo):

            diccionario = {
                "mag": data_json["features"][i]["properties"]["mag"],
                "place": data_json["features"][i]["properties"]["place"],
                "time": data_json["features"][i]["properties"]["time"],
                "updated": data_json["features"][i]["properties"]["updated"],
                "alert": data_json["features"][i]["properties"]["alert"],
                "status": data_json["features"][i]["properties"]["status"],
                "tsunami": data_json["features"][i]["properties"]["tsunami"],
                "magType": data_json["features"][i]["properties"]["magType"],
                "type": data_json["features"][i]["properties"]["type"],
                "title": data_json["features"][i]["properties"]["title"],
            }

            finalList.append(diccionario)
            
        data_tuple = [(datetime.datetime.now() , fecha_inicio, fecha_fin, magnitude_minima , 0, json.dumps(finalList) )]
            
        # Insert a row of data
        conn.executemany("INSERT INTO earthquake VALUES (?,?,?,?,?,?);", data_tuple)
            
        # Save (commit) the changes
        conn.commit()
    
    except:
        
        print('error')

    finally:
        
        # close connection
        conn.close()

    return json.dumps(finalList)




@app.route("/searchEarthquake/getEarthquakesByMagnitudes", methods=['POST'])
def getEarthquakesByMagnitudes ():

    try:
        conn = sqlite3.connect(':memory')

        # Create table
        conn.execute('''CREATE TABLE IF NOT EXISTS earthquake
                    (create_at timestamp PRIMARY KEY, 
                    fecha_inicio timestamp, 
                    fecha_fin timestamp, 
                    magnitud_min real , 
                    magnitud_max real,
                    salida text)''')

        req_data = request.get_json()
        magnitude_minima = req_data['magnitudeMinima']
        magnitude_maxima = req_data['magnitudeMaxima']
        response = requests.get('https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&minmagnitude={}&maxmagnitude={}'.format(magnitude_minima, magnitude_maxima), verify=False)
        data_json = json.loads(response.text)
        largo = len(data_json["features"])
        finalList = []

        for i in range(largo):

            diccionario = {
                "mag": data_json["features"][i]["properties"]["mag"],
                "place": data_json["features"][i]["properties"]["place"],
                "time": data_json["features"][i]["properties"]["time"],
                "updated": data_json["features"][i]["properties"]["updated"],
                "alert": data_json["features"][i]["properties"]["alert"],
                "status": data_json["features"][i]["properties"]["status"],
                "tsunami": data_json["features"][i]["properties"]["tsunami"],
                "magType": data_json["features"][i]["properties"]["magType"],
                "type": data_json["features"][i]["properties"]["type"],
                "title": data_json["features"][i]["properties"]["title"],
            }

            finalList.append(diccionario)
        
        data_tuple = [(datetime.datetime.now() , '', '', magnitude_minima , magnitude_maxima, json.dumps(finalList) )]
            
        # Insert a row of data
        conn.executemany("INSERT INTO earthquake VALUES (?,?,?,?,?,?);", data_tuple)
            
        # Save (commit) the changes
        conn.commit()
    
    except:
        print('Error!')

    finally:
        # close connection
        conn.close()

    return json.dumps(finalList)


if __name__ == '__main__':
    app.run(debug=True, port=8089, host='localhost')